import HeaderItem, { HeaderItemProps } from "./HeaderItem";
import {
  BadgeCheckIcon,
  CollectionIcon,
  HomeIcon,
  LightningBoltIcon,
  SearchIcon,
  UserIcon,
} from "@heroicons/react/outline"; // outline and solid versions
import Image from "next/image";

const Header = () => {
  const icons: HeaderItemProps[] = [
    { title: "HOME", HeroIcon: HomeIcon },
    { title: "TRENDING", HeroIcon: LightningBoltIcon },
    { title: "VERIFIED", HeroIcon: BadgeCheckIcon },
    { title: "COLLECTIONS", HeroIcon: CollectionIcon },
    { title: "SEARCH", HeroIcon: SearchIcon },
    { title: "ACCOUNT", HeroIcon: UserIcon },
  ];

  return (
    <header className="flex flex-col sm:flex-row m-5 justify-between h-auto">
      <div className="flex flex-grow justify-evenly max-w-2xl">
        {icons &&
          icons.map((icon: HeaderItemProps) => (
            <HeaderItem title={icon.title} HeroIcon={icon.HeroIcon} />
          ))}
      </div>
      <Image src="/hulu.png" width={200} height={100} />
    </header>
  );
};

export default Header;
