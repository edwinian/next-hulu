import { SVGProps } from "react";

export interface HeaderItemProps {
  title: string;
  HeroIcon: (props: SVGProps<SVGSVGElement>) => JSX.Element;
}

const HeaderItem = ({ title, HeroIcon }: HeaderItemProps) => {
  return (
    <div className="group flex flex-col items-center cursor-pointer w-12 sm:w-20">
      <HeroIcon className="h-7 mb-1 group-hover:animate-bounce text-white" />
      {/* tracking-widest: space out the text */}
      {/* group-hover: style when the whole div is hovered */}
      <p className="opacity-0 group-hover:opacity-100 group-hover:text-white tracking-widest text-white-500">
        {title}
      </p>
    </div>
  );
};

export default HeaderItem;
