import { useRouter } from "next/dist/client/router"
import requests from "../utils/requests"

const Nav = () => {
    const router = useRouter()
    
    return (<nav className='relative'>
        <div className="flex px-10 sm:px-20 text-2xl whitespace-nowrap space-x-10 sm:space-x-20 overflow-x-scroll scrollbar-hide">
            {Object.entries(requests).map(([key, {title, url}]) =>(
                // last:pr-24 = add 24 padding-right to the last element
                <h2 key={key} onClick={() => router.push(`/?genre=${key}`)} className="last:pr-24 text-white cursor-pointer transition duration-100 transform hover:scale-125 hover:text-red-500 active:text-red-80000">{title}</h2>
            ))}
        </div>
        {/* gradient to transparent color if not given */}
        <div className='absolute top-0 right-0 bg-gradient-to-l h-10 w-1/12'/>
    </nav>)
}

export default Nav