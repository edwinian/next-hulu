import axios from "axios";
import Head from "next/head";
import Header from '../components/Header'
import Nav from '../components/Nav'
import Results from '../components/Results'
import requests from "../utils/requests";

export default function Home(props) {

  console.log("Results, ", props.results);

  return (
    <div>
      <Head>
        <title>Hulu</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header/>
      <Nav />

      {/* Results */}
      <Results results={props.results}/>
    </div>
  );
}

// This gets executed before Home on the client side
export const getServerSideProps = async(context) => {
  const genre = context.query.genre

  const request = await axios.get(`https://api.themoviedb.org/3${requests[genre]?.url || requests.fetchTrending.url}`).then(res => res.data);

  return {
    props: {results: request.results}
  }
}

