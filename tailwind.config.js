module.exports = {
  // mode: 'jit',
  purge: [], // clean up all css not used
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        "3xl": "2000px"
      }
    },
  },
  variants: {
    extend: {},
  },
  // tailwind-related libs
  plugins: [require('tailwind-scrollbar-hide')],
}
